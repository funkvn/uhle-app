import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: 'Uhle App',
    initialRoute: '/',
    routes: {
      '/': (context) => HomeScreen(),
      '/saturdays': (context) => SaturdaysScreen(),
      '/holidays': (context) => HolidaysScreen(),
      '/events': (context) => EventsScreen(),
    },
  ));
}

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.amber[400],
        title: Text('Uhle App'),
      ),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/background.jpg"),
            fit: BoxFit.cover
          )
        ),
        child: Center(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                RaisedButton(
                  child: Text('Ferien- und Feiertage'),
                  onPressed: () {
                    Navigator.pushNamed(context, '/holidays');
                  },
                ),
                RaisedButton(
                  child: Text('Schulsamstage'),
                  onPressed: () {
                    // Navigate to the second screen using a named route.
                    Navigator.pushNamed(context, '/saturdays');
                  },
                ),
                RaisedButton(
                  child: Text('Schulveranstaltungen'),
                  onPressed: () {
                    Navigator.pushNamed(context, '/events');
                  },
                ),
              ]
          )
        )
      )
    );
  }
}

class HolidaysScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.amber[400],
        title: Text("Ferienplan"),
      ),
      body: Center(
          child: Text('Schulfrei!!!')
      ),
    );
  }
}

class SaturdaysScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.amber[400],
          title: Text("Schulsamstage"),
        ),
        body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  color: Colors.orange,
                  child: FlutterLogo(
                    size: 60.0,
                  ),
                ),
                Container(
                  color: Colors.blue,
                  child: FlutterLogo(
                    size: 60.0,
                  ),
                ),
                Container(
                  color: Colors.purple,
                  child: FlutterLogo(
                    size: 60.0,
                  ),
                ),
              ],
            )
        )
    );
  }
}

class EventsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.amber[400],
        title: Text("Schulveranstaltungen"),
      ),
      body: Center(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                RichText(
                  text: TextSpan(
                    text: 'Theaterprojekt der 12. Klasse:\n ',
                    style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black),
                    children: <TextSpan>[
                      TextSpan(text: 'Fr. 22.11.2019\n'),
                      TextSpan(text: 'Sa. 23.11.2019\n '),
                      TextSpan(text: 'So. 24.11.2019\n'),
                    ],
                  ),
                ),
                RichText(
                    text: TextSpan(
                        text: '\nAdventsbazar:\n ',
                        style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black),
                        children: <TextSpan>[
                          TextSpan(text: 'Sa. 30.11.2019'),
                        ]))
              ]
          )
      ),
    );
  }
}
